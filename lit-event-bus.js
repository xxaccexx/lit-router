const listeners = new Map();

export class LitEventBus {
	constructor() {
		listeners.set(this, []);
	}

	get on() {
		return (event, handler) => {
			this.addListener(event, handler, false);
		}
	}

	get once() {
		return (event, handler) => {
			this.addListener(event, handler, true);
		}
	}

	get addListener() {
		return (event, handler, once) => {
			listeners.get(this).push({ event, handler, once });
		}
	}

	get emit() {
		return (event, ...values) => {
			const matchingHandlers = listeners.get(this)
				.filter(obj => obj.event == event);

			const remaining = listeners.get(this)
				.filter(obj => {
					if (!matchingHandlers.includes(obj)) return true;
					if (!obj.once) return true;
					return false;
				});

			Promise.resolve()
				.then(() => {
					matchingHandlers.forEach(obj => obj.handler.call(null, ...values));
					listeners.set(this, remaining);
				});
		}
	}
}