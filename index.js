import { LitEventBus } from './lit-event-bus';
import { join } from '@acce/lit-path';

import './url-pattern';


const resolveRoute = (url, route) => {
	return {
		url,
		route,
		data: route.pattern.match(url)
	}
}

const resolveRouteList = (url, routes) => {
	return routes.flatMap(route => ([
		resolveRoute(url, route),
		...resolveRouteList(url, route.children)
	]), Infinity);
}

const deepCloneRoutes = (route, parentPath) => {
	let {
		path = '',
		children = [],
		before = () => true,
		action = () => undefined
	} = route;

	if (typeof parentPath == 'string' && parentPath.length)
		path = join(parentPath.path, path);

	const pattern = path ?
		new UrlPattern(path) :
		{ match: () => null };

	children = children.map((child) => deepCloneRoutes(child, path));

	//	final result
	return { path, pattern, before, action, children };
}

export class LitRouter extends LitEventBus {
	constructor(routes) {
		super();

		this.__root = routes.map(deepCloneRoutes);

		document.addEventListener('click', (e) => {
			e.preventDefault();
			this.__navigate(this.__resolveNavigation(e), true);
		});

		window.addEventListener('popstate', (e) => {
			this.__navigate(this.__findRoute(location.pathname));
		});

		document.addEventListener('DOMContentLoaded', () => {
			this.__navigate(this.__findRoute(location.pathname));
		});
	}

	async __navigate(state, pushState = false) {
		if (!state) return;

		if (pushState === true)
			history.pushState(state.data, '', state.url);

		await state.route.action(state.data);

		this.emit('navigate', state.url, state.data);
	}

	__resolveNavigation(e) {
		let href = e.path.reduce((final, elem) => {
			if (!(elem instanceof HTMLAnchorElement)) return final;
			if (!elem.pathname || elem.pathname === location.pathname) return final;
			return elem.pathname;
		}, undefined);

		if (href) {
			return this.__findRoute(href);
		}
	}

	__findRoute(url) {
		let routeEvents = resolveRouteList(url, this.__root)
		const result = routeEvents.find(route => route.data);
		return result;
	}

	redirect(url) {
		this.__navigate(this.__findRoute(url), true);
	}
}
